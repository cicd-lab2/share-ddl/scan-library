class EmailNotifications {
    static void successNotification() {
        emailext (
            to: 'dieulinht.t2403t.t@gmail.com',
            subject: "SUCCESS: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
            body: "The job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' completed successfully. You can view the build details (${env.BUILD_URL})."
        )
    }

    static void failureNotification() {
        emailext (
            to: 'dieulinht.t2403t.t@gmail.com',
            subject: "FAILURE: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
            body: "The job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' failed. You can view the build details (${env.BUILD_URL})."
        )
    }
}