def call (Map config=[:]){
    withSonarQubeEnv (installationName: "${env.InstallationName}", credentialsId:"${env.CredentialsId}" )
    {
        sh "./mvnw clean package sonar:sonar -Dcheckstyle.skip -Dskiptest "
    }
}