def call(Map config = [:] ){
    def BUILD_TIMESTAMP = sh(script: "echo `date +%s`", returnStdout: true).trim()
          if ("${config.groupId}" == "UAT"){
        nexusArtifactUploader( 
            artifacts:
            [[artifactId: 'spring-petclinic', classifier: '', 
            file: './target/spring-petclinic-3.1.0-SNAPSHOT.jar', 
            type: 'jar']], 
            credentialsId: 'nexus', 
            groupId: 'UAT', 
            nexusUrl: '192.168.20.20:8081', 
            nexusVersion: 'nexus3', 
            protocol: 'http', 
            repository: 'Petclinic-UAT', 
            version: "${env.BUILD_ID}_${BUILD_TIMESTAMP}_snapshot"
        )
        }else{
            nexusArtifactUploader( 
            artifacts:
            [[artifactId: 'spring-petclinic', classifier: '', 
            file: './target/spring-petclinic-3.1.0-SNAPSHOT.jar', 
            type: 'jar']], 
            credentialsId: 'nexus', 
            groupId: 'release', 
            nexusUrl: '192.168.20.20:8081', 
            nexusVersion: 'nexus3', 
            protocol: 'http', 
            repository: 'Petclinic-RELEASE', 
            version: "${env.BUILD_ID}_${BUILD_TIMESTAMP}_release"
        )
        }
//     def artifact_custom = 'UAT/spring-petclinic/petclinic.jar'
//     withCredentials([
//             usernamePassword(credentialsId: 'nexus', usernameVariable:"${env.user_nexus}" , passwordVariable: "${env.password_nexus}")
//             ]) {
//             sh "
            curl -v -u 'your_username:your_password' -X POST \
        -F "r=Petclinic-UAT" \
        -F "g=UAT" \
        -F "a=spring-petclinic" \
        -F "v=${env.BUILD_ID}_${BUILD_TIMESTAMP}_snapshot" \
        -F "p=jar" \
        -F "file=@./target/spring-petclinic-3.1.0-SNAPSHOT.jar" \
        http://192.168.20.20:8081/repository/nexus3/

// curl -v -u ${env.user_nexus}:${env.password_nexus} --upload-file ${config.artifactPath} ${config.nexusUrl}${artifact_custom}"
//             }
    
    }



