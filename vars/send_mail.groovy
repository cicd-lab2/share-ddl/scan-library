def call(Map config = [:]) {
    load 'email_notification.groovy'

    if (currentBuild.result = 'SUCCESS') {
        email_notification.successNotification()
    } else if (currentBuild.result = 'FAILURE') {
        email_notification.failureNotification()
    }
}